package controllers;

import models.Artista;
import models.Cancion;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.artista.buscarArtista;
import views.html.index;

import javax.inject.Inject;


//Controlador Principal
public class HomeController extends Controller {

    @Inject
    private FormFactory defaultForm;

    //Renderizado del view principal
    public Result index() {
        return ok(index.render());
    }

    public Result buscarArtista(){
        Form<Artista> formArtista = defaultForm.form(Artista.class);
        return ok(buscarArtista.render(formArtista));
    }

}

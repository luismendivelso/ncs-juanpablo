# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table artista (
  usuario                       varchar(255) not null,
  pass                          varchar(255),
  nombre                        varchar(255),
  edad                          integer not null,
  correo                        varchar(255),
  constraint pk_artista primary key (usuario)
);

create table cancion (
  id                            serial not null,
  artista                       varchar(255) not null,
  nombre                        varchar(255) not null,
  genero                        varchar(255),
  direccion                     varchar(255) not null,
  constraint pk_cancion primary key (id)
);


# --- !Downs

drop table if exists artista cascade;

drop table if exists cancion cascade;


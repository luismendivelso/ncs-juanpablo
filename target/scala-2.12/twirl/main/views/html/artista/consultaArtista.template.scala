
package views.html.artista

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object consultaArtista extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template2[Artista,List[Cancion],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(artista: Artista, canciones: List[Cancion]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.46*/("""

"""),_display_(/*3.2*/layout("Home")/*3.16*/ {_display_(Seq[Any](format.raw/*3.18*/("""
    """),format.raw/*4.5*/("""<div class="jumbotron">
            <h1>"""),_display_(/*5.18*/artista/*5.25*/.getNombre),format.raw/*5.35*/("""</h1>
            <h3>"""),_display_(/*6.18*/artista/*6.25*/.getUsuario),format.raw/*6.36*/("""</h3>
            <h3>"""),_display_(/*7.18*/artista/*7.25*/.getCorreoElectronico),format.raw/*7.46*/("""</h3>
            <h3>"""),_display_(/*8.18*/artista/*8.25*/.getEdad),format.raw/*8.33*/(""" """),format.raw/*8.34*/("""Años</h3>
    </div>
    <div class="col-sm-5">
    """),_display_(/*11.6*/for(song <- canciones) yield /*11.28*/ {_display_(Seq[Any](format.raw/*11.30*/("""
        """),format.raw/*12.9*/("""<h3><a href=""""),_display_(/*12.23*/routes/*12.29*/.ControladorCancion.mostrarCancion(song.getId)),format.raw/*12.75*/("""">
            Cancion: """),_display_(/*13.23*/song/*13.27*/.getNombre),format.raw/*13.37*/("""</a></h3>
        <h4><a href=""""),_display_(/*14.23*/routes/*14.29*/.ControladorCancion.eliminarCancion(song.getId)),format.raw/*14.76*/("""">
            Eliminar
        </a></h4>
    """)))}),format.raw/*17.6*/("""
    """),format.raw/*18.5*/("""</div>
    <div class="col-sm6">
        <a class="btn btn-success" href=""""),_display_(/*20.43*/routes/*20.49*/.ControladorCancion.agregarCancion(artista.getUsuario)),format.raw/*20.103*/(""""> Agregar Canción</a>
        <a class="btn btn-danger" href=""""),_display_(/*21.42*/routes/*21.48*/.ControladorArtista.editar(artista.getUsuario)),format.raw/*21.94*/(""""> Editar Perfil</a>
        <a class="btn btn-danger" href=""""),_display_(/*22.42*/routes/*22.48*/.ControladorArtista.confirmacion(artista.getUsuario)),format.raw/*22.100*/(""""> Eliminar Cuenta</a>
    </div>
""")))}))
      }
    }
  }

  def render(artista:Artista,canciones:List[Cancion]): play.twirl.api.HtmlFormat.Appendable = apply(artista,canciones)

  def f:((Artista,List[Cancion]) => play.twirl.api.HtmlFormat.Appendable) = (artista,canciones) => apply(artista,canciones)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Oct 30 12:06:34 COT 2017
                  SOURCE: C:/Users/juanp/Desktop/negromano-ncsongs-juanpablo-1fee771e0695/app/views/artista/consultaArtista.scala.html
                  HASH: b2e9419490e0e9c204d9e2b505d80b6bf8c3a963
                  MATRIX: 981->1|1120->45|1148->48|1170->62|1209->64|1240->69|1307->110|1322->117|1352->127|1401->150|1416->157|1447->168|1496->191|1511->198|1552->219|1601->242|1616->249|1644->257|1672->258|1751->311|1789->333|1829->335|1865->344|1906->358|1921->364|1988->410|2040->435|2053->439|2084->449|2143->481|2158->487|2226->534|2303->581|2335->586|2437->661|2452->667|2528->721|2619->785|2634->791|2701->837|2790->899|2805->905|2879->957
                  LINES: 28->1|33->1|35->3|35->3|35->3|36->4|37->5|37->5|37->5|38->6|38->6|38->6|39->7|39->7|39->7|40->8|40->8|40->8|40->8|43->11|43->11|43->11|44->12|44->12|44->12|44->12|45->13|45->13|45->13|46->14|46->14|46->14|49->17|50->18|52->20|52->20|52->20|53->21|53->21|53->21|54->22|54->22|54->22
                  -- GENERATED --
              */
          
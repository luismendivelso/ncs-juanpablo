
package views.html.artista

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object agregarArtista extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Form[Artista],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(form: Form[Artista]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.23*/("""

"""),_display_(/*3.2*/layout("Registro")/*3.20*/ {_display_(Seq[Any](format.raw/*3.22*/("""

    """),format.raw/*5.5*/("""<div class="container">
        <div class="col-sm-3">
        """),_display_(/*7.10*/helper/*7.16*/.form(action = helper.CSRF(routes.ControladorArtista.guardar()))/*7.80*/ {_display_(Seq[Any](format.raw/*7.82*/("""

            """),_display_(/*9.14*/helper/*9.20*/.inputText(form("Usuario"))),format.raw/*9.47*/("""
            """),_display_(/*10.14*/helper/*10.20*/.inputPassword(form("Pass"))),format.raw/*10.48*/("""
            """),_display_(/*11.14*/helper/*11.20*/.inputText(form("Nombre"))),format.raw/*11.46*/("""
            """),_display_(/*12.14*/helper/*12.20*/.inputText(form("Correo"))),format.raw/*12.46*/("""
            """),_display_(/*13.14*/helper/*13.20*/.inputText(form("Edad"))),format.raw/*13.44*/("""

            """),format.raw/*15.13*/("""<input type="submit" value="Registrar" class="btn btn-success">
            """)))}),format.raw/*16.14*/("""
        """),format.raw/*17.9*/("""</div>
        <div class="col-sm-5">
            <h2 align="center"> ¡Regístrate!</h2>
            <p align="justify">
                Únete a la nueva comunidad de artistas On-Line, sube tus temas, date a conocer a ti y a tus creaciones.</p>
        </div>
        <div class="col-sm-3">
            <img src=""""),_display_(/*24.24*/routes/*24.30*/.Assets.versioned("images/ncslogo.png")),format.raw/*24.69*/("""" width="248" height="106">
        </div>
    </div>
""")))}))
      }
    }
  }

  def render(form:Form[Artista]): play.twirl.api.HtmlFormat.Appendable = apply(form)

  def f:((Form[Artista]) => play.twirl.api.HtmlFormat.Appendable) = (form) => apply(form)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Oct 30 12:06:34 COT 2017
                  SOURCE: C:/Users/juanp/Desktop/negromano-ncsongs-juanpablo-1fee771e0695/app/views/artista/agregarArtista.scala.html
                  HASH: 1103e0bcc937b8032bbe2bcbdd36f01bfaa9dd21
                  MATRIX: 972->1|1088->22|1116->25|1142->43|1181->45|1213->51|1303->115|1317->121|1389->185|1428->187|1469->202|1483->208|1530->235|1571->249|1586->255|1635->283|1676->297|1691->303|1738->329|1779->343|1794->349|1841->375|1882->389|1897->395|1942->419|1984->433|2092->510|2128->519|2468->832|2483->838|2543->877
                  LINES: 28->1|33->1|35->3|35->3|35->3|37->5|39->7|39->7|39->7|39->7|41->9|41->9|41->9|42->10|42->10|42->10|43->11|43->11|43->11|44->12|44->12|44->12|45->13|45->13|45->13|47->15|48->16|49->17|56->24|56->24|56->24
                  -- GENERATED --
              */
          
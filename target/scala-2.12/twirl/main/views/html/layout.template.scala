
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object layout extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template2[String,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(titulo: String)(body: Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.30*/("""

"""),format.raw/*3.1*/("""<html>
    <head>
        <link rel="stylesheet" media="screen" href=""""),_display_(/*5.54*/routes/*5.60*/.Assets.versioned("stylesheets/bootstrap.min.css")),format.raw/*5.110*/("""">
        <link rel="stylesheet" media="screen" href=""""),_display_(/*6.54*/routes/*6.60*/.Assets.versioned("stylesheets/bootstrap-theme.min.css")),format.raw/*6.116*/("""">
        <link rel="stylesheet" media="screen" href=""""),_display_(/*7.54*/routes/*7.60*/.Assets.versioned("stylesheets/style.css")),format.raw/*7.102*/("""">

        <link rel="shortcut icon" type="image/png" href=""""),_display_(/*9.59*/routes/*9.65*/.Assets.versioned("images/favicon.png")),format.raw/*9.104*/("""">

        <title>"""),_display_(/*11.17*/titulo),format.raw/*11.23*/("""</title>
    </head>
    <body>

        <header>

            <div class="navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="exnav">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="" class="navbar-brand"> NCSongs</a>
                    </div>
                    <div class="collapse navbar-collapse" id="exnav">
                        <ul class="nav navbar-nav navbar-left">
                            <li><a href="/">Home</a></li>
                            <li><a href=""""),_display_(/*30.43*/routes/*30.49*/.HomeController.buscarArtista()),format.raw/*30.80*/("""">Busca un artista existente</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
        <div class="container">
            """),_display_(/*37.14*/if(flash.containsKey("danger"))/*37.45*/{_display_(Seq[Any](format.raw/*37.46*/("""
                """),format.raw/*38.17*/("""<div class="alert-danger">
                     """),_display_(/*39.23*/flash/*39.28*/.get("danger")),format.raw/*39.42*/("""
                """),format.raw/*40.17*/("""</div>
            """)))}),format.raw/*41.14*/("""

        """),format.raw/*43.9*/("""</div>
        <div class="container">
        """),_display_(/*45.10*/body),format.raw/*45.14*/("""
        """),format.raw/*46.9*/("""</div>

        <script src=""""),_display_(/*48.23*/routes/*48.29*/.Assets.versioned("javascripts/jquery-3.2.1.min.js")),format.raw/*48.81*/("""" type="text/javascript"></script>
        <script src=""""),_display_(/*49.23*/routes/*49.29*/.Assets.versioned("javascripts/bootstrap.min.js")),format.raw/*49.78*/("""" type="text/javascript"></script>
        <script src=""""),_display_(/*50.23*/routes/*50.29*/.Assets.versioned("javascripts/custom.js")),format.raw/*50.71*/("""" type="text/javascript"></script>
    </body>
</html>"""))
      }
    }
  }

  def render(titulo:String,body:Html): play.twirl.api.HtmlFormat.Appendable = apply(titulo)(body)

  def f:((String) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (titulo) => (body) => apply(titulo)(body)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Oct 30 12:06:34 COT 2017
                  SOURCE: C:/Users/juanp/Desktop/negromano-ncsongs-juanpablo-1fee771e0695/app/views/layout.scala.html
                  HASH: 9e74305024d96cda8ea381c4e308cdc00252b133
                  MATRIX: 954->1|1077->29|1105->31|1202->102|1216->108|1287->158|1369->214|1383->220|1460->276|1542->332|1556->338|1619->380|1707->442|1721->448|1781->487|1828->507|1855->513|2722->1353|2737->1359|2789->1390|3016->1590|3056->1621|3095->1622|3140->1639|3216->1688|3230->1693|3265->1707|3310->1724|3361->1744|3398->1754|3473->1802|3498->1806|3534->1815|3591->1845|3606->1851|3679->1903|3763->1960|3778->1966|3848->2015|3932->2072|3947->2078|4010->2120
                  LINES: 28->1|33->1|35->3|37->5|37->5|37->5|38->6|38->6|38->6|39->7|39->7|39->7|41->9|41->9|41->9|43->11|43->11|62->30|62->30|62->30|69->37|69->37|69->37|70->38|71->39|71->39|71->39|72->40|73->41|75->43|77->45|77->45|78->46|80->48|80->48|80->48|81->49|81->49|81->49|82->50|82->50|82->50
                  -- GENERATED --
              */
          
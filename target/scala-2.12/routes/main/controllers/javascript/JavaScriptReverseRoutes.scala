
// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/juanp/Desktop/negromano-ncsongs-juanpablo-1fee771e0695/conf/routes
// @DATE:Mon Oct 30 11:12:56 COT 2017

import play.api.routing.JavaScriptReverseRoute


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers.javascript {

  // @LINE:12
  class ReverseControladorArtista(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:24
    def editar: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ControladorArtista.editar",
      """
        function(usuario0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "editar" + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("usuario", usuario0)])})
        }
      """
    )
  
    // @LINE:28
    def confirmacion: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ControladorArtista.confirmacion",
      """
        function(usuario0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "confirmar" + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("usuario", usuario0)])})
        }
      """
    )
  
    // @LINE:14
    def mostrarArtista: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ControladorArtista.mostrarArtista",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "login"})
        }
      """
    )
  
    // @LINE:20
    def guardar: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ControladorArtista.guardar",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "registro"})
        }
      """
    )
  
    // @LINE:18
    def agregar: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ControladorArtista.agregar",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "registro"})
        }
      """
    )
  
    // @LINE:26
    def actualizar: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ControladorArtista.actualizar",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "editar"})
        }
      """
    )
  
    // @LINE:30
    def eliminar: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ControladorArtista.eliminar",
      """
        function(usuario0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "eliminar" + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("usuario", usuario0)])})
        }
      """
    )
  
    // @LINE:38
    def buscarArtista: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ControladorArtista.buscarArtista",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "artista"})
        }
      """
    )
  
    // @LINE:22
    def mostrarTodos: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ControladorArtista.mostrarTodos",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "todos"})
        }
      """
    )
  
    // @LINE:12
    def login: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ControladorArtista.login",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "login"})
        }
      """
    )
  
  }

  // @LINE:6
  class ReverseHomeController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:36
    def buscarArtista: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.buscarArtista",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "artista"})
        }
      """
    )
  
    // @LINE:6
    def index: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.index",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + """"})
        }
      """
    )
  
  }

  // @LINE:16
  class ReverseAssets(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:16
    def versioned: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.versioned",
      """
        function(file1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[play.api.mvc.PathBindable[Asset]].javascriptUnbind + """)("file", file1)})
        }
      """
    )
  
  }

  // @LINE:8
  class ReverseControladorCancion(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:8
    def agregarCancion: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ControladorCancion.agregarCancion",
      """
        function(artista0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "agregarCancion" + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("artista", artista0)])})
        }
      """
    )
  
    // @LINE:34
    def eliminarCancion: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ControladorCancion.eliminarCancion",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "eliminarCancion" + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[Integer]].javascriptUnbind + """)("id", id0)])})
        }
      """
    )
  
    // @LINE:32
    def mostrarCancion: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ControladorCancion.mostrarCancion",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "cancion" + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[Integer]].javascriptUnbind + """)("id", id0)])})
        }
      """
    )
  
    // @LINE:10
    def upload: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ControladorCancion.upload",
      """
        function(artista0) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "agregarCancion" + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("artista", artista0)])})
        }
      """
    )
  
  }


}

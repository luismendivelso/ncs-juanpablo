
// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/juanp/Desktop/negromano-ncsongs-juanpablo-1fee771e0695/conf/routes
// @DATE:Mon Oct 30 11:12:56 COT 2017

import play.api.mvc.Call


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers {

  // @LINE:12
  class ReverseControladorArtista(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:24
    def editar(usuario:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "editar" + play.core.routing.queryString(List(Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("usuario", usuario)))))
    }
  
    // @LINE:28
    def confirmacion(usuario:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "confirmar" + play.core.routing.queryString(List(Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("usuario", usuario)))))
    }
  
    // @LINE:14
    def mostrarArtista(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "login")
    }
  
    // @LINE:20
    def guardar(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "registro")
    }
  
    // @LINE:18
    def agregar(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "registro")
    }
  
    // @LINE:26
    def actualizar(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "editar")
    }
  
    // @LINE:30
    def eliminar(usuario:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "eliminar" + play.core.routing.queryString(List(Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("usuario", usuario)))))
    }
  
    // @LINE:38
    def buscarArtista(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "artista")
    }
  
    // @LINE:22
    def mostrarTodos(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "todos")
    }
  
    // @LINE:12
    def login(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "login")
    }
  
  }

  // @LINE:6
  class ReverseHomeController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:36
    def buscarArtista(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "artista")
    }
  
    // @LINE:6
    def index(): Call = {
      
      Call("GET", _prefix)
    }
  
  }

  // @LINE:16
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:16
    def versioned(file:Asset): Call = {
      implicit lazy val _rrc = new play.core.routing.ReverseRouteContext(Map(("path", "/public"))); _rrc
      Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[play.api.mvc.PathBindable[Asset]].unbind("file", file))
    }
  
  }

  // @LINE:8
  class ReverseControladorCancion(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:8
    def agregarCancion(artista:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "agregarCancion" + play.core.routing.queryString(List(Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("artista", artista)))))
    }
  
    // @LINE:34
    def eliminarCancion(id:Integer): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "eliminarCancion" + play.core.routing.queryString(List(Some(implicitly[play.api.mvc.QueryStringBindable[Integer]].unbind("id", id)))))
    }
  
    // @LINE:32
    def mostrarCancion(id:Integer): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "cancion" + play.core.routing.queryString(List(Some(implicitly[play.api.mvc.QueryStringBindable[Integer]].unbind("id", id)))))
    }
  
    // @LINE:10
    def upload(artista:String): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "agregarCancion" + play.core.routing.queryString(List(Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("artista", artista)))))
    }
  
  }


}

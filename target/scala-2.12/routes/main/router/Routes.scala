
// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/juanp/Desktop/negromano-ncsongs-juanpablo-1fee771e0695/conf/routes
// @DATE:Mon Oct 30 11:12:56 COT 2017

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:6
  HomeController_0: controllers.HomeController,
  // @LINE:8
  ControladorCancion_1: controllers.ControladorCancion,
  // @LINE:12
  ControladorArtista_3: controllers.ControladorArtista,
  // @LINE:16
  Assets_2: controllers.Assets,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:6
    HomeController_0: controllers.HomeController,
    // @LINE:8
    ControladorCancion_1: controllers.ControladorCancion,
    // @LINE:12
    ControladorArtista_3: controllers.ControladorArtista,
    // @LINE:16
    Assets_2: controllers.Assets
  ) = this(errorHandler, HomeController_0, ControladorCancion_1, ControladorArtista_3, Assets_2, "/")

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, HomeController_0, ControladorCancion_1, ControladorArtista_3, Assets_2, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.HomeController.index"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """agregarCancion""", """controllers.ControladorCancion.agregarCancion(artista:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """agregarCancion""", """controllers.ControladorCancion.upload(artista:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """login""", """controllers.ControladorArtista.login()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """login""", """controllers.ControladorArtista.mostrarArtista()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """registro""", """controllers.ControladorArtista.agregar()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """registro""", """controllers.ControladorArtista.guardar()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """todos""", """controllers.ControladorArtista.mostrarTodos()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """editar""", """controllers.ControladorArtista.editar(usuario:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """editar""", """controllers.ControladorArtista.actualizar()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """confirmar""", """controllers.ControladorArtista.confirmacion(usuario:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """eliminar""", """controllers.ControladorArtista.eliminar(usuario:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """cancion""", """controllers.ControladorCancion.mostrarCancion(id:Integer)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """eliminarCancion""", """controllers.ControladorCancion.eliminarCancion(id:Integer)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """artista""", """controllers.HomeController.buscarArtista()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """artista""", """controllers.ControladorArtista.buscarArtista()"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:6
  private[this] lazy val controllers_HomeController_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_HomeController_index0_invoker = createInvoker(
    HomeController_0.index,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "index",
      Nil,
      "GET",
      this.prefix + """""",
      """ Página principal""",
      Seq()
    )
  )

  // @LINE:8
  private[this] lazy val controllers_ControladorCancion_agregarCancion1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("agregarCancion")))
  )
  private[this] lazy val controllers_ControladorCancion_agregarCancion1_invoker = createInvoker(
    ControladorCancion_1.agregarCancion(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ControladorCancion",
      "agregarCancion",
      Seq(classOf[String]),
      "GET",
      this.prefix + """agregarCancion""",
      """ Agrega una Canción a un artista""",
      Seq()
    )
  )

  // @LINE:10
  private[this] lazy val controllers_ControladorCancion_upload2_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("agregarCancion")))
  )
  private[this] lazy val controllers_ControladorCancion_upload2_invoker = createInvoker(
    ControladorCancion_1.upload(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ControladorCancion",
      "upload",
      Seq(classOf[String]),
      "POST",
      this.prefix + """agregarCancion""",
      """ Agrega una canción a un artista""",
      Seq()
    )
  )

  // @LINE:12
  private[this] lazy val controllers_ControladorArtista_login3_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("login")))
  )
  private[this] lazy val controllers_ControladorArtista_login3_invoker = createInvoker(
    ControladorArtista_3.login(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ControladorArtista",
      "login",
      Nil,
      "GET",
      this.prefix + """login""",
      """ Login de un artista ya registrado""",
      Seq()
    )
  )

  // @LINE:14
  private[this] lazy val controllers_ControladorArtista_mostrarArtista4_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("login")))
  )
  private[this] lazy val controllers_ControladorArtista_mostrarArtista4_invoker = createInvoker(
    ControladorArtista_3.mostrarArtista(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ControladorArtista",
      "mostrarArtista",
      Nil,
      "POST",
      this.prefix + """login""",
      """ Validación y login con los datos provistos""",
      Seq()
    )
  )

  // @LINE:16
  private[this] lazy val controllers_Assets_versioned5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned5_invoker = createInvoker(
    Assets_2.versioned(fakeValue[String], fakeValue[Asset]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      this.prefix + """assets/""" + "$" + """file<.+>""",
      """""",
      Seq()
    )
  )

  // @LINE:18
  private[this] lazy val controllers_ControladorArtista_agregar6_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("registro")))
  )
  private[this] lazy val controllers_ControladorArtista_agregar6_invoker = createInvoker(
    ControladorArtista_3.agregar(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ControladorArtista",
      "agregar",
      Nil,
      "GET",
      this.prefix + """registro""",
      """ Registro de un nuevo artista""",
      Seq()
    )
  )

  // @LINE:20
  private[this] lazy val controllers_ControladorArtista_guardar7_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("registro")))
  )
  private[this] lazy val controllers_ControladorArtista_guardar7_invoker = createInvoker(
    ControladorArtista_3.guardar(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ControladorArtista",
      "guardar",
      Nil,
      "POST",
      this.prefix + """registro""",
      """ POST del registro del artista""",
      Seq()
    )
  )

  // @LINE:22
  private[this] lazy val controllers_ControladorArtista_mostrarTodos8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("todos")))
  )
  private[this] lazy val controllers_ControladorArtista_mostrarTodos8_invoker = createInvoker(
    ControladorArtista_3.mostrarTodos(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ControladorArtista",
      "mostrarTodos",
      Nil,
      "GET",
      this.prefix + """todos""",
      """ Muestra todos los artistas registrados""",
      Seq()
    )
  )

  // @LINE:24
  private[this] lazy val controllers_ControladorArtista_editar9_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("editar")))
  )
  private[this] lazy val controllers_ControladorArtista_editar9_invoker = createInvoker(
    ControladorArtista_3.editar(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ControladorArtista",
      "editar",
      Seq(classOf[String]),
      "GET",
      this.prefix + """editar""",
      """ Edita la información de un usuario identificado""",
      Seq()
    )
  )

  // @LINE:26
  private[this] lazy val controllers_ControladorArtista_actualizar10_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("editar")))
  )
  private[this] lazy val controllers_ControladorArtista_actualizar10_invoker = createInvoker(
    ControladorArtista_3.actualizar(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ControladorArtista",
      "actualizar",
      Nil,
      "POST",
      this.prefix + """editar""",
      """ Actualiza la información del artista""",
      Seq()
    )
  )

  // @LINE:28
  private[this] lazy val controllers_ControladorArtista_confirmacion11_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("confirmar")))
  )
  private[this] lazy val controllers_ControladorArtista_confirmacion11_invoker = createInvoker(
    ControladorArtista_3.confirmacion(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ControladorArtista",
      "confirmacion",
      Seq(classOf[String]),
      "GET",
      this.prefix + """confirmar""",
      """ Condirma la eliminación de la cuenta""",
      Seq()
    )
  )

  // @LINE:30
  private[this] lazy val controllers_ControladorArtista_eliminar12_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("eliminar")))
  )
  private[this] lazy val controllers_ControladorArtista_eliminar12_invoker = createInvoker(
    ControladorArtista_3.eliminar(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ControladorArtista",
      "eliminar",
      Seq(classOf[String]),
      "GET",
      this.prefix + """eliminar""",
      """ Elimina la cuenta""",
      Seq()
    )
  )

  // @LINE:32
  private[this] lazy val controllers_ControladorCancion_mostrarCancion13_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("cancion")))
  )
  private[this] lazy val controllers_ControladorCancion_mostrarCancion13_invoker = createInvoker(
    ControladorCancion_1.mostrarCancion(fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ControladorCancion",
      "mostrarCancion",
      Seq(classOf[Integer]),
      "GET",
      this.prefix + """cancion""",
      """ Muestra una canción sin haberse identificado""",
      Seq()
    )
  )

  // @LINE:34
  private[this] lazy val controllers_ControladorCancion_eliminarCancion14_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("eliminarCancion")))
  )
  private[this] lazy val controllers_ControladorCancion_eliminarCancion14_invoker = createInvoker(
    ControladorCancion_1.eliminarCancion(fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ControladorCancion",
      "eliminarCancion",
      Seq(classOf[Integer]),
      "GET",
      this.prefix + """eliminarCancion""",
      """ Elimina una canción desde la cuenta del artista""",
      Seq()
    )
  )

  // @LINE:36
  private[this] lazy val controllers_HomeController_buscarArtista15_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("artista")))
  )
  private[this] lazy val controllers_HomeController_buscarArtista15_invoker = createInvoker(
    HomeController_0.buscarArtista(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "buscarArtista",
      Nil,
      "GET",
      this.prefix + """artista""",
      """ Busca a un artista sin derecho a editar""",
      Seq()
    )
  )

  // @LINE:38
  private[this] lazy val controllers_ControladorArtista_buscarArtista16_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("artista")))
  )
  private[this] lazy val controllers_ControladorArtista_buscarArtista16_invoker = createInvoker(
    ControladorArtista_3.buscarArtista(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ControladorArtista",
      "buscarArtista",
      Nil,
      "POST",
      this.prefix + """artista""",
      """ Busca a un artista sin derechos a editar""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:6
    case controllers_HomeController_index0_route(params@_) =>
      call { 
        controllers_HomeController_index0_invoker.call(HomeController_0.index)
      }
  
    // @LINE:8
    case controllers_ControladorCancion_agregarCancion1_route(params@_) =>
      call(params.fromQuery[String]("artista", None)) { (artista) =>
        controllers_ControladorCancion_agregarCancion1_invoker.call(ControladorCancion_1.agregarCancion(artista))
      }
  
    // @LINE:10
    case controllers_ControladorCancion_upload2_route(params@_) =>
      call(params.fromQuery[String]("artista", None)) { (artista) =>
        controllers_ControladorCancion_upload2_invoker.call(ControladorCancion_1.upload(artista))
      }
  
    // @LINE:12
    case controllers_ControladorArtista_login3_route(params@_) =>
      call { 
        controllers_ControladorArtista_login3_invoker.call(ControladorArtista_3.login())
      }
  
    // @LINE:14
    case controllers_ControladorArtista_mostrarArtista4_route(params@_) =>
      call { 
        controllers_ControladorArtista_mostrarArtista4_invoker.call(ControladorArtista_3.mostrarArtista())
      }
  
    // @LINE:16
    case controllers_Assets_versioned5_route(params@_) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned5_invoker.call(Assets_2.versioned(path, file))
      }
  
    // @LINE:18
    case controllers_ControladorArtista_agregar6_route(params@_) =>
      call { 
        controllers_ControladorArtista_agregar6_invoker.call(ControladorArtista_3.agregar())
      }
  
    // @LINE:20
    case controllers_ControladorArtista_guardar7_route(params@_) =>
      call { 
        controllers_ControladorArtista_guardar7_invoker.call(ControladorArtista_3.guardar())
      }
  
    // @LINE:22
    case controllers_ControladorArtista_mostrarTodos8_route(params@_) =>
      call { 
        controllers_ControladorArtista_mostrarTodos8_invoker.call(ControladorArtista_3.mostrarTodos())
      }
  
    // @LINE:24
    case controllers_ControladorArtista_editar9_route(params@_) =>
      call(params.fromQuery[String]("usuario", None)) { (usuario) =>
        controllers_ControladorArtista_editar9_invoker.call(ControladorArtista_3.editar(usuario))
      }
  
    // @LINE:26
    case controllers_ControladorArtista_actualizar10_route(params@_) =>
      call { 
        controllers_ControladorArtista_actualizar10_invoker.call(ControladorArtista_3.actualizar())
      }
  
    // @LINE:28
    case controllers_ControladorArtista_confirmacion11_route(params@_) =>
      call(params.fromQuery[String]("usuario", None)) { (usuario) =>
        controllers_ControladorArtista_confirmacion11_invoker.call(ControladorArtista_3.confirmacion(usuario))
      }
  
    // @LINE:30
    case controllers_ControladorArtista_eliminar12_route(params@_) =>
      call(params.fromQuery[String]("usuario", None)) { (usuario) =>
        controllers_ControladorArtista_eliminar12_invoker.call(ControladorArtista_3.eliminar(usuario))
      }
  
    // @LINE:32
    case controllers_ControladorCancion_mostrarCancion13_route(params@_) =>
      call(params.fromQuery[Integer]("id", None)) { (id) =>
        controllers_ControladorCancion_mostrarCancion13_invoker.call(ControladorCancion_1.mostrarCancion(id))
      }
  
    // @LINE:34
    case controllers_ControladorCancion_eliminarCancion14_route(params@_) =>
      call(params.fromQuery[Integer]("id", None)) { (id) =>
        controllers_ControladorCancion_eliminarCancion14_invoker.call(ControladorCancion_1.eliminarCancion(id))
      }
  
    // @LINE:36
    case controllers_HomeController_buscarArtista15_route(params@_) =>
      call { 
        controllers_HomeController_buscarArtista15_invoker.call(HomeController_0.buscarArtista())
      }
  
    // @LINE:38
    case controllers_ControladorArtista_buscarArtista16_route(params@_) =>
      call { 
        controllers_ControladorArtista_buscarArtista16_invoker.call(ControladorArtista_3.buscarArtista())
      }
  }
}

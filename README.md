# README #

Repositorio de la aplicación en [Heroku](https://ncs-juan.herokuapp.com/)

## Funciones: ##

* Creación de Artistas nuevos como usuarios
* Búsqueda de Artistas registrados
* Creación de nuevas canciones subiendo un archivo en formato mp3
* Los archivos se respaldan en Cloudinary
* Para hacer modificaciones en el perfil, agregar canciones o eliminar el perfil del artista es necesario autenticarse
* La vista de /todos muestra todos los artistas registrados junto con sus respectivos usuarios y contraseñas (Esto solo con fines de prueba)

## Características ##

* Ya hay 4 artistas registrados, tobu, elektronimia, alanwalker y pentakill. Todos con dos canciones. La contraseña para todos es 12345

# Juan Pablo Rodríguez Morales #
### 2220171070@estudiantesunibague.edu.co###
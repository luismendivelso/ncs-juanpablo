package models;

import org.junit.Test;
import play.test.WithApplication;

import static org.junit.Assert.assertEquals;

public class ArtistaTest extends WithApplication {

    private Artista artista;

    public void setEscenario1(){
        artista = new Artista();
    }
    public void setEscenario2(){
        artista = new Artista("AC/DC","12345");
    }
    public void setEscenario3(){
        artista = new Artista("AC/DC","12345","AC/DC",44,"mail@acdc.com");
    }

    @Test
    public void testAgregacion(){

        setEscenario1();
        assertEquals("No debería haber ningún valor asignado", null , artista.getUsuario());
        assertEquals("No debería haber ningún valor asignado", null , artista.getPass());

        setEscenario2();
        assertEquals("Debería haber un valor asignado", "AC/DC" , artista.getUsuario());
        assertEquals("Debería haber un valor asignado", "12345" , artista.getPass());

        setEscenario3();
        assertEquals("El Correo no es correcto", "mail@acdc.com" , artista.getCorreoElectronico());
        assertEquals("El nombre no es correcto", "AC/DC" , artista.getNombre());
        assertEquals("La edad es incorrecta", 44 , artista.getEdad());

    }

}
